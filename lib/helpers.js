/**
 * Adds spaced between numbers. 1000000 becomes "1 000 000"
 *
 * @param {Number|String} nr Number to add spaces between
 *
 * @returns {String} Number with spaced
 */
export const numberWithSpaces = (nr = 0) => {
  const parts = nr.toString().split('.')
  return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ' ') || 0
}

/**
 * Slugifies string. "hurr&durr! inc." becomes "hurr-and-durr-inc"
 *
 * @param {String} string String to slugify
 *
 * @returns {String} Slugified string
 */
export const slugify = (string = '') => {
  const a = 'àáäâãåăæąçćčđďèéěėëêęğǵḧìíïîįłḿǹńňñòóöôœøṕŕřßşśšșťțùúüûǘůűūųẃẍÿýźžż·/_,:;'
  const b = 'aaaaaaaaacccddeeeeeeegghiiiiilmnnnnooooooprrsssssttuuuuuuuuuwxyyzzz------'
  const p = new RegExp(a.split('').join('|'), 'g')

  return string.toString().toLowerCase()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
    .replace(/&/g, '-and-') // Replace & with 'and'
    .replace(/[^\w-]+/g, '') // Remove all non-word characters
    .replace(/--+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, '') // Trim - from end of text
}
