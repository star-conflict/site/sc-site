import axios from 'axios'

function getHeaders () {
  let headers = {
    accept: 'application/json',
    'content-type': 'application/json'
  }
  if (window.localStorage.sc_site) {
    const parsed = JSON.parse(window.localStorage.sc_site)
    if (parsed.auth && parsed.auth.access_token) {
      headers = {
        ...headers,
        authorization: 'Bearer ' + parsed.auth.access_token
      }
    }
  }
  return headers
}

const Axios = axios.create({
  baseURL: process.env.NUXT_ENV_API_URL
})

Axios.interceptors.request.use((config) => {
  config.headers = {
    ...config.headers,
    ...getHeaders()
  }
  return config
})

Axios.interceptors.response.use(null, function (error) {
  if (error.response.status === 401) {
    // Do something with 401
  }
  return Promise.reject(error)
})

export default Axios
