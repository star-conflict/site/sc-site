export default {
  welcome: null,
  steam_players_note: null,
  buttons: {
    search: null
  },
  migration_broken_data: null,
  level: null,
  player: {
    link: null,
    search_user: null,
    username: null,
    activity_chart_title: null,
    win_loss_chart_title: null,
    update_description: null,
    no_player: null,
    // General
    efficiency: null,
    fleet_str: null,
    karma: null,
    raw_stats: null,
    // General games
    games_played: null,
    games_won: null,
    games_lost: null,
    win_lose_ration: null,
    time_in_battle: null,
    hours: null
  },
  coop: {
    coop: null,
    no_coop: null
  },
  pve: {
    pve: null,
    attack_level: null,
    defence_level: null,
    avg_mission_level: null,
    no_pve: null,
    levels: {
      defenceContract: null,
      thePriceOfTrust: null,
      ariadnesThread: null,
      blackwoodShipyard: null,
      operationIceBelt: null,
      operationMonolith: null,
      capturedDreadnought: null,
      fireSupport: null,
      processingRig: null,
      ellydiumPlantRaid: null,
      operationCrimsonHaze: null,
      templeOfLastHope: null,
      pirateFortRaid: null
    }
  },
  pvp: {
    pvp: null,
    kills_total: null,
    deaths_total: null,
    kill_death_ratio: null,
    kills_per_battle: null,
    assists_total: null,
    assist_death_ratio: null,
    assist_per_battle: null,
    damage_total: null,
    damage_per_battle: null,
    healing_total: null,
    healing_per_battle: null,
    capture_points: null,
    capture_points_per_battle: null,
    no_pvp: null
  },
  corp: {
    link: null,
    search_corp: null,
    tag: null
  }
}
