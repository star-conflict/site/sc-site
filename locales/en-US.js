export default {
  welcome: 'Welcome',
  steam_players_note: 'Note! This is players who play thro Steam only!',
  buttons: {
    search: 'Search'
  },
  migration_broken_data: 'Due to data migration historical charts might be off between 25-29 of December 2019',
  level: 'Level',
  player: {
    link: 'Players',
    search_user: 'Search for player',
    username: 'Username',
    activity_chart_title: 'Player activity (Does not count OS)',
    win_loss_chart_title: 'Total win / loss ratio',
    update_description: 'Data updates once a day',
    no_player: 'No such player: ',
    // General
    efficiency: 'Efficiency',
    fleet_str: 'Fleet strength',
    karma: 'Karma',
    raw_stats: 'Raw stats (up to date)',
    // General games
    games_played: 'Games played',
    games_won: 'Games won',
    games_lost: 'Games lost',
    win_lose_ration: 'Win / loss ratio',
    time_in_battle: 'Time in battle',
    hours: 'hours'
  },
  coop: {
    coop: 'CO-OP',
    no_coop: 'No COOP battles played'
  },
  pve: {
    pve: 'PvE',
    attack_level: 'Attack level',
    defence_level: 'Defence level',
    avg_mission_level: 'Average mission level',
    no_pve: 'No PvE battles played',
    levels: {
      defenceContract: 'Defence Contract',
      thePriceOfTrust: 'The price of trust',
      ariadnesThread: 'Ariadne\'s Thread',
      blackwoodShipyard: '\'Blackwood\' Shipyard',
      operationIceBelt: 'Operation \'Ice Belt\'',
      operationMonolith: 'Operation \'Monolith\'',
      capturedDreadnought: 'Captured Dreadnought',
      fireSupport: 'Fire support',
      processingRig: 'Processing rig',
      ellydiumPlantRaid: '\'Ellydium\' plant raid',
      operationCrimsonHaze: 'Operation «Crimson Haze»',
      templeOfLastHope: 'Temple of Last hope',
      pirateFortRaid: 'Pirate Fort Raid'
    }
  },
  pvp: {
    pvp: 'PvP',
    kills_total: 'Total kills',
    deaths_total: 'Total deaths',
    kill_death_ratio: 'Kill / death ratio',
    kills_per_battle: 'Kills per battle',
    assists_total: 'Total assists',
    assist_death_ratio: 'Assist / death ratio',
    assist_per_battle: 'Assists per battle',
    damage_total: 'Total damage',
    damage_per_battle: 'Damage per battle',
    healing_total: 'Total healing',
    healing_per_battle: 'Healing per battle',
    capture_points: 'Capture points',
    capture_points_per_battle: 'Capture points per battle',
    no_pvp: 'No PvP battles played'
  },
  corp: {
    link: 'Corporations',
    search_corp: 'Search for corporation',
    tag: 'Tag'
  }
}
