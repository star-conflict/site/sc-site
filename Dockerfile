FROM node:14-alpine

ENV HOST=0.0.0.0
ENV PORT=80

ARG NUXT_ENV_API_URL
ENV NUXT_ENV_API_URL=$NUXT_ENV_API_URL

EXPOSE 80

# Add working dir
WORKDIR /opt/sc-site

# Copy over packages
COPY package*.json ./

# Install locker package versions
RUN npm ci

ENV NODE_ENV=production

# Copy over source code
COPY . .

RUN npm run build

CMD ["npm", "start"]
