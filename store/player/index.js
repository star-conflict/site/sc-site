import Vue from 'vue'
import Axios from '@/lib/axios'

export const state = () => ({
  player: null,
  history: null,
  customKey: 'coop.gamePlayed'
})

export const getters = {
  pve: (state) => {
    return (state.player || {}).pve
  },
  pvp: (state) => {
    return (state.player || {}).pvp
  },
  coop: (state) => {
    return (state.player || {}).coop
  },
  openWorld: (state) => {
    return (state.player || {}).openWorld
  },
  clan: (state) => {
    return (state.player || {}).clan
  },
  karmaLevel: (state, getters) => {
    const karma = (getters.openWorld || {}).karma
    if (karma >= 0) {
      if (karma < 1000) {
        return 0
      }
      if (karma < 10000) {
        return 1
      }
      if (karma < 50000) {
        return 2
      }
      if (karma < 150000) {
        return 3
      }
      if (karma < 500000) {
        return 4
      }
      if (karma >= 500000) {
        return 5
      }
    } else {
      if (karma > -1000) {
        return 0
      }
      if (karma > -10000) {
        return -1
      }
      if (karma > -50000) {
        return -2
      }
      if (karma > -150000) {
        return -3
      }
      if (karma > -500000) {
        return -4
      }
      if (karma <= -500000) {
        return -5
      }
    }
    return 0
  },
  customKey: (state) => {
    return state.customKey
  }
}

export const mutations = {
  SET_PLAYER (state, player) {
    state.player = player
  },
  SET_HISTORY (state, history) {
    state.history = history
  },
  SET_CUSTOM_KEY (state, value) {
    Vue.set(state, 'customKey', value)
  }
}

export const actions = {
  getPlayer ({ commit }, name = '') {
    return Axios.get(`/proxy/plain/${name}`)
      .then(({ data }) => {
        commit('SET_PLAYER', data)
        return data
      })
  },

  /**
   * Get player history by UID
   *
   * @param {ctx} ctx Store
   * @param {Number} uid Player UID
   */
  getHistory ({ commit }, uid = 0) {
    return Axios.get(`/players/history/${uid}`)
      .then(({ data }) => {
        commit('SET_HISTORY', data.reverse())
        return data
      })
  }
}
